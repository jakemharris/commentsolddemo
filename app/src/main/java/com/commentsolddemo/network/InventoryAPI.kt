package com.commentsolddemo.network

import com.commentsolddemo.model.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface InventoryAPI {

    @GET("status")
    fun authenticate(): Call<AuthenticateResponse>

    @GET("products")
    fun getProducts(): Call<GetProductsResponse>

    @POST("product")
    fun addNewProduct(@Body body: NewProductDto ): Call<NewProductResponse>

}
