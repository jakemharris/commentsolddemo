package com.commentsolddemo.network

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit


object RetrofitController {
    private const val BASE_URL: String = "https://cscodetest.herokuapp.com/api/"

    fun getBasicRetrofitInstance(username: String, password: String): Retrofit {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.readTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.writeTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.callTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.connectTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.addInterceptor(loggingInterceptor)
        okHttpBuilder.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "Basic ${getBasicAuth(username, password)}")
                .build()
            chain.proceed(request)
        }

        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpBuilder.build())
            .build()
    }

    fun getSecuredRetrofitInstance(token: String): Retrofit {
        Log.d("jake", "getSecuredRetrofitInstance token== " + token)
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.readTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.writeTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.callTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.connectTimeout(10, TimeUnit.SECONDS)
        okHttpBuilder.addInterceptor(loggingInterceptor)
        okHttpBuilder.addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $token")
                .build()
            chain.proceed(request)
        }

        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpBuilder.build())
            .build()
    }

    private fun getBasicAuth(username: String, password: String): String {
        val auth = Base64.getEncoder().encodeToString("$username:$password".toByteArray())
        Log.d("jake", " getBasicAuth= $auth")
        return auth
    }
}
