package com.commentsolddemo

import java.text.NumberFormat
import java.util.regex.Pattern

object Utils {

    const val PRODUCT_KEY = "PRODUCT_KEY"
    val EMAIL_ADDRESS_PATTERN = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )


    fun validateEmail(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

    fun validateEmailOrPhone(emailOrPhone: String): Boolean {

        val phone = try {
            emailOrPhone.toLong()
        } catch (e: Exception) {
            0L
        }

        return if (phone > 0 && emailOrPhone.length == 10) {
            true
        } else {
            validateEmail(emailOrPhone)
        }
    }

    fun getCurrencyFormat(number: Long?): String {
        number?.let {
            val reducedDouble = it/100
            val numberFormat = NumberFormat.getCurrencyInstance()
            numberFormat.maximumFractionDigits = 2
            return numberFormat.format(reducedDouble)
        }
        return "$0.00"
    }

}
