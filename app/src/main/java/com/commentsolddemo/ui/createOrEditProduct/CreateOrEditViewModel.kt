package com.commentsolddemo.ui.createOrEditProduct

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.commentsolddemo.ui.repositories.AuthenticateRepository
import com.commentsolddemo.ui.repositories.InventoryRepository
import com.commentsolddemo.model.NewProductDto
import com.commentsolddemo.model.NewProductResponse
import com.commentsolddemo.network.NetworkResult
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CreateOrEditViewModel(private val authenticateRepository: AuthenticateRepository, private val inventoryRepository: InventoryRepository) : ViewModel() {

    val mCreateEditProductDataModel = CreateEditProductDataModel(false, null, null,false)
    val dataModelLiveData = MutableLiveData<CreateEditProductDataModel>()

     fun addNewProduct(newProduct: NewProductDto) {
        mCreateEditProductDataModel.isLoading = true
        mCreateEditProductDataModel.errorMessage =null
        mCreateEditProductDataModel.successMessage = null
        dataModelLiveData.postValue(mCreateEditProductDataModel)
        viewModelScope.launch(Dispatchers.IO) {
            val result = try {
                inventoryRepository.createNewProduct(newProduct, authenticateRepository.authToken)
            } catch (e: Exception) {
                NetworkResult.Error(e)
            }
            mCreateEditProductDataModel.isLoading = false
            when (result) {
                is NetworkResult.Success<NewProductResponse?> -> {
                    Log.d("jake", "create product success in VM = " + Gson().toJson(result.data))
                    mCreateEditProductDataModel.errorMessage = null
                    mCreateEditProductDataModel.successMessage = result.data?.message
                    mCreateEditProductDataModel.isComplete = true
                }
                is NetworkResult.Error -> {
                    Log.d("jake", "create product error in VM = ${result.exception.message}")
                    mCreateEditProductDataModel.errorMessage = result.exception.message
                    mCreateEditProductDataModel.successMessage = null
                    mCreateEditProductDataModel.isComplete = false
                }
            }
            dataModelLiveData.postValue(mCreateEditProductDataModel)
        }
    }

    data class CreateEditProductDataModel(
        var isLoading: Boolean,
        var errorMessage: String?,
        var successMessage: String?,
        var isComplete:Boolean
    )

    class CreateOrEditViewModelFactory(private val authRepository: AuthenticateRepository, private val inventoryRepository: InventoryRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = CreateOrEditViewModel(authRepository, inventoryRepository) as T
    }
}
