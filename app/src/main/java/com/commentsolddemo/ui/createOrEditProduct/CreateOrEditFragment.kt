package com.commentsolddemo.ui.createOrEditProduct

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.commentsolddemo.R
import com.commentsolddemo.databinding.FragmentCreateEditProductBinding
import com.commentsolddemo.model.NewProductDto
import com.commentsolddemo.ui.BaseFragment
import com.commentsolddemo.ui.repositories.AuthenticateRepository
import com.commentsolddemo.ui.repositories.InventoryRepository

class CreateOrEditFragment : BaseFragment() {
    private val viewModel: CreateOrEditViewModel by viewModels { CreateOrEditViewModel.CreateOrEditViewModelFactory(AuthenticateRepository.getInstance(requireContext()), InventoryRepository.getInstance(requireContext())) }
    private var _binding: FragmentCreateEditProductBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentCreateEditProductBinding.inflate(inflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.dataModelLiveData.observe(viewLifecycleOwner, { uiData ->
            uiData?.let {
                if (it.isLoading) {
                    disableButton()
                    binding.pbLoading.visibility = VISIBLE
                } else {
                    activateButton()
                    binding.pbLoading.visibility = GONE
                }

                it.successMessage?.let { message ->
                    showSnackMessage(message, view)
                }
                it.errorMessage?.let { message ->
                    showSnackMessage(message, view)
                }
                if (it.isComplete) {
                    binding.bAddItem.text = "Done"
                } else {
                    binding.bAddItem.text = "Add item"
                }
            }
        })

        binding.bAddItem.setOnClickListener {
            if (binding.bAddItem.text == "Done") {
                activity?.onBackPressed()
            } else {
                createItem()
            }
        }
    }

    private fun createItem() {
        val name = binding.etName.text.toString()
        val description = binding.etDescription.text.toString()
        val style = binding.etStyle.text.toString()
        val brand = binding.etBrand.text.toString()
        val shippingCostCents = binding.etShippingCostCents.text.toString()

        if (name.isBlank()) {
            binding.tilName.error = "Please enter valid name"
        } else {
            binding.tilName.error = null
        }
        if (description.isBlank()) {
            binding.tilDescription.error = "Please enter valid description"
        } else {
            binding.tilDescription.error = null
        }
        if (style.isBlank()) {
            binding.tilStyle.error = "Please enter valid style, must not be blank"
        } else {
            binding.tilStyle.error = null
        }
        if (brand.isBlank()) {
            binding.tilBrand.error = "Please enter valid brand, must not be blank"
        } else {
            binding.tilBrand.error = null
        }
        if (shippingCostCents.isBlank()) {
            binding.tilShippingCost.error = "Please enter valid shipping cost in cents"
        } else {
            binding.tilShippingCost.error = null
        }

        view?.let { context?.hideKeyboard(it) }
        if (name.isNotBlank() && description.isNotBlank() && style.isNotBlank() && brand.isNotBlank() && shippingCostCents.isNotBlank()) {
            viewModel.addNewProduct(NewProductDto(name, description, style, brand, shipping_price_cents = shippingCostCents.toLong()))
        }
    }

    private fun activateButton() {
        binding.bAddItem.isClickable = true
        binding.bAddItem.setBackgroundColor(resources.getColor(R.color.dark_blue))
    }

    private fun disableButton() {
        binding.bAddItem.isClickable = false
        binding.bAddItem.setBackgroundColor(resources.getColor(R.color.silver))
    }


//    private fun setData(items: List<ArticlesEntity>) {
//        binding.rv.apply {
//            hasFixedSize()
//            layoutManager = LinearLayoutManager(this.context)
//            adapter = NewsFeedAdapter(requireContext(), items, this@NewsFeedFragment)
//        }
//    }


//    fun navigateToDetailsPage() {
//        findNavController().navigate(R.id.action_news_feed_to_details)
//    }


    companion object {
        fun newInstance() = CreateOrEditFragment()
    }
}
