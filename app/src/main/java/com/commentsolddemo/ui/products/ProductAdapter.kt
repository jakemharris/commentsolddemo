package com.commentsolddemo.ui.products


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.commentsolddemo.R
import com.commentsolddemo.Utils
import com.commentsolddemo.Utils.getCurrencyFormat
import com.commentsolddemo.model.Product


class ProductAdapter(var context: Context, var items: List<Product>, var onProductClickListener: OnProductClickListener) : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return ViewHolder(view, onProductClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        val title = if (item.product_name.isNullOrBlank()) item.description else item.product_name
        val cost = "Shipping cost: ${getCurrencyFormat(item.shipping_price)}"
        holder.tvName.text = title
        holder.tvCost.text = cost
//        Glide.with(context).load(item.urlToImage).into(holder.ivIcon)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View, private var onItemClick: OnProductClickListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var tvName: TextView = itemView.findViewById(R.id.tvName)
        var tvCost: TextView = itemView.findViewById(R.id.tvCost)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            onItemClick.onProductClickListener(items[adapterPosition])
        }
    }
}

interface OnProductClickListener {
    fun onProductClickListener(product: Product)
}
