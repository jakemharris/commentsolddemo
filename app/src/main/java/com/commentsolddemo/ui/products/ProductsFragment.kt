package com.commentsolddemo.ui.products


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.commentsolddemo.R
import com.commentsolddemo.Utils.PRODUCT_KEY
import com.commentsolddemo.databinding.FragmentProductsBinding
import com.commentsolddemo.model.Product
import com.commentsolddemo.ui.BaseFragment
import com.commentsolddemo.ui.repositories.AuthenticateRepository
import com.commentsolddemo.ui.repositories.InventoryRepository
import com.google.gson.Gson

class ProductsFragment : BaseFragment(), OnProductClickListener {

    private val viewModel: ProductsViewModel by viewModels { ProductsViewModel.ProductViewModelFactory(AuthenticateRepository.getInstance(requireContext()), InventoryRepository.getInstance(requireContext())) }
    private var _binding: FragmentProductsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductsBinding.inflate(inflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.productsViewStateLiveData.observe(viewLifecycleOwner, { uiData ->
            uiData?.let {
                binding.pbLoading.visibility = if (it.isLoading) VISIBLE else GONE
                binding.tvEmptyState.visibility = if (it.products.isNullOrEmpty()) VISIBLE else GONE
                it.errorMessage?.let { errorMessage ->
                    showSnackMessage(errorMessage, view)
                }
                setProductData(it.products)
            }
        })

        binding.fab.setOnClickListener {
            navigateToNewItemPage()
        }
    }

    private fun setProductData(products: List<Product>?) {
        products?.let {
            if (it.isNotEmpty()) {
                binding.rv.apply {
                    hasFixedSize()
                    layoutManager = LinearLayoutManager(this.context)
                    adapter = ProductAdapter(requireContext(), it, this@ProductsFragment)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.getProducts()
    }

    private fun navigateToDetailsPage(product: Product) {
        val bundle = Bundle()
        bundle.putString(PRODUCT_KEY, Gson().toJson(product))
        findNavController().navigate(R.id.action_products_to_details, bundle)
    }

    private fun navigateToNewItemPage() {
        findNavController().navigate(R.id.action_products_to_create_edit)
    }

    override fun onProductClickListener(product: Product) {
        Log.d("jake", "clicking item in fragment: $product")
        navigateToDetailsPage(product)
    }

    companion object {
        fun newInstance() = ProductsFragment()
    }

}
