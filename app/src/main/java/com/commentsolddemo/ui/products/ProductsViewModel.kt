package com.commentsolddemo.ui.products


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.commentsolddemo.ui.repositories.AuthenticateRepository
import com.commentsolddemo.ui.repositories.InventoryRepository
import com.commentsolddemo.model.GetProductsResponse
import com.commentsolddemo.model.Product
import com.commentsolddemo.network.NetworkResult
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductsViewModel(private val authenticateRepository: AuthenticateRepository, private val inventoryRepository: InventoryRepository) : ViewModel() {

    val productsViewStateLiveData = MutableLiveData<ProductsViewStateModel>()
    private val mProductsViewStateModel = ProductsViewStateModel(false, null, false, null)


     fun getProducts() {
        mProductsViewStateModel.isLoading = true
        productsViewStateLiveData.postValue(mProductsViewStateModel)
        viewModelScope.launch(Dispatchers.IO) {
            val result = try {
                inventoryRepository.getProducts(authenticateRepository.authToken)
            } catch (e: Exception) {
                NetworkResult.Error(e)
            }
            mProductsViewStateModel.isLoading = false
            when (result) {
                is NetworkResult.Success<GetProductsResponse?> -> {
                    Log.d("jake", "get products success in VM = " + Gson().toJson(result.data))
                    mProductsViewStateModel.errorMessage = null
                    mProductsViewStateModel.products = result.data?.products
                }
                is NetworkResult.Error -> {
                    Log.d("jake", "get products error in VM = ${result.exception.message}")
                    mProductsViewStateModel.errorMessage = result.exception.message
                }
            }
            productsViewStateLiveData.postValue(mProductsViewStateModel)
        }
    }

    data class ProductsViewStateModel(
        var isLoading: Boolean,
        var products: List<Product>?,
        var showEmptyState: Boolean,
        var errorMessage: String?
    )

    class ProductViewModelFactory(private val authRepository: AuthenticateRepository, private val inventoryRepository: InventoryRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = ProductsViewModel(authRepository, inventoryRepository) as T
    }
}
