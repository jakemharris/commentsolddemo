package com.commentsolddemo.ui.repositories

import android.content.Context
import com.commentsolddemo.model.AuthenticateResponse
import com.commentsolddemo.network.InventoryAPI
import com.commentsolddemo.network.NetworkResult
import com.commentsolddemo.network.RetrofitController
import retrofit2.awaitResponse

class AuthenticateRepository private constructor(context: Context?) {

    var authToken = ""

    suspend fun authenticateUser(username: String, password: String): NetworkResult<AuthenticateResponse?> {
        val retrofitController = RetrofitController.getBasicRetrofitInstance(username, password).create(InventoryAPI::class.java)
        val responses = retrofitController.authenticate().awaitResponse()
        return if (responses.body()?.token != null) {
            authToken = responses.body()?.token.orEmpty()
            NetworkResult.Success(responses.body())
        } else {
            NetworkResult.Error(Exception(responses.message()))
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: AuthenticateRepository? = null

        fun getInstance(context: Context?): AuthenticateRepository {
            return INSTANCE ?: synchronized(this) {
                INSTANCE?.let {
                    return it
                }

                val instance = AuthenticateRepository(context)
                INSTANCE = instance
                instance
            }
        }
    }
}
