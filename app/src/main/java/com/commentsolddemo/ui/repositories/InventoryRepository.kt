package com.commentsolddemo.ui.repositories

import android.content.Context
import com.commentsolddemo.model.GetProductsResponse
import com.commentsolddemo.model.NewProductDto
import com.commentsolddemo.model.NewProductResponse
import com.commentsolddemo.network.InventoryAPI
import com.commentsolddemo.network.NetworkResult
import com.commentsolddemo.network.RetrofitController
import retrofit2.awaitResponse

class InventoryRepository private constructor(context: Context?) {

    suspend fun getProducts(token: String): NetworkResult<GetProductsResponse?> {
        val retrofitController = RetrofitController.getSecuredRetrofitInstance(token).create(InventoryAPI::class.java)
        val response = retrofitController.getProducts().awaitResponse()
        return if (response.body() != null) {
            NetworkResult.Success(response.body())
        } else {
            NetworkResult.Error(Exception(response.message()))
        }
    }

    suspend fun createNewProduct(newProductDto: NewProductDto, token: String): NetworkResult<NewProductResponse?> {
        val retrofitController = RetrofitController.getSecuredRetrofitInstance(token).create(InventoryAPI::class.java)
        val response = retrofitController.addNewProduct(newProductDto).awaitResponse()
        return if (response.body() != null) {
            NetworkResult.Success(response.body())
        } else {
            NetworkResult.Error(Exception(response.message()))
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: InventoryRepository? = null

        fun getInstance(context: Context?): InventoryRepository {
            return INSTANCE ?: synchronized(this) {
                INSTANCE?.let {
                    return it
                }

                val instance = InventoryRepository(context)
                INSTANCE = instance
                instance
            }
        }
    }
}
