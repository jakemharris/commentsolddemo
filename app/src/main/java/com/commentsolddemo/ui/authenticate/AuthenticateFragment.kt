package com.commentsolddemo.ui.authenticate


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.commentsolddemo.ui.repositories.AuthenticateRepository
import com.commentsolddemo.R
import com.commentsolddemo.databinding.FragmentAuthenticateBinding
import com.commentsolddemo.ui.BaseFragment

class AuthenticateFragment : BaseFragment() {
    private val viewModel: AuthenticateViewModel by viewModels { AuthenticateViewModel.AuthenticateViewModelFactory(AuthenticateRepository.getInstance(requireContext())) }
    private var _binding: FragmentAuthenticateBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentAuthenticateBinding.inflate(inflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.loadingLiveData.observe(viewLifecycleOwner, { visible ->
            binding.pbLoading.visibility = if (visible) VISIBLE else GONE
        })

        viewModel.tokenLiveData.observe(viewLifecycleOwner, { token ->
            token?.let {
                activateButton()
                navigateToProducts()
            }
        })

        viewModel.errorMessageLiveData.observe(viewLifecycleOwner, { error ->
            error?.let {
                showSnackMessage(error, view)
                activateButton()
            }
        })

        binding.bSignIn.setOnClickListener {
            signIn()
        }

        binding.tvLogin.setOnClickListener {
            getView()?.let { it1 -> showSnackMessage("jake", it1) }
        }
        binding.etUsername.setText("hortensia.dollison@blargmail.org")
        binding.etPassword.setText("AgcGcJxeig")
    }

    private fun activateButton() {
        binding.bSignIn.isClickable = true
        binding.bSignIn.setBackgroundColor(resources.getColor(R.color.dark_blue))
    }

    private fun disableButton() {
        binding.bSignIn.isClickable = false
        binding.bSignIn.setBackgroundColor(resources.getColor(R.color.silver))
    }

    private fun signIn() {
        disableButton()
        val username = binding.etUsername.text.toString()
        val password = binding.etPassword.text.toString()
        if (username.isBlank()) {
            binding.tilUsername.error = "Please enter valid username"
        } else {
            binding.tilUsername.error = null
        }
        if (password.isBlank()) {
            binding.tilPassword.error = "Please enter valid password"
        } else {
            binding.tilPassword.error = null
        }
        if (username.isNotBlank() && password.isNotBlank()) {
            view?.let { requireContext().hideKeyboard(it) }
            viewModel.authenticate(username = username, password = password)
        } else {
            activateButton()
        }
    }

    private fun navigateToProducts() {
        viewModel.tokenLiveData.postValue(null)
        findNavController().navigate(R.id.action_auth_to_products)
    }


    companion object {
        fun newInstance() = AuthenticateFragment()
    }
}
