package com.commentsolddemo.ui.authenticate


import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.commentsolddemo.ui.repositories.AuthenticateRepository
import com.commentsolddemo.model.AuthenticateResponse
import com.commentsolddemo.network.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AuthenticateViewModel(private val authenticateRepository: AuthenticateRepository) : ViewModel() {

    val loadingLiveData = MutableLiveData<Boolean>()
    val tokenLiveData = MutableLiveData<String>()
    val errorMessageLiveData = MutableLiveData<String>()


    fun authenticate(username: String, password: String) {
        Log.d("jake", "authenticate() vm username $username password:$password")
        loadingLiveData.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            val result = try {
                authenticateRepository.authenticateUser(username, password)
            } catch (e: Exception) {
                NetworkResult.Error(e)
            }
            loadingLiveData.postValue(false)
            when (result) {
                is NetworkResult.Success<AuthenticateResponse?> -> {
                    //success
                    if (!result.data?.token.isNullOrBlank()) {
                        tokenLiveData.postValue(result.data?.token)
                        Log.d("jake", "token ${result.data?.token}")
                    } else {
                        errorMessageLiveData.postValue("Unable to authenticate at this time, please try again.")
                    }
                }
                is NetworkResult.Error -> {
                    errorMessageLiveData.postValue("Server error. Message: " + result.exception.message)
                }
            }
        }
    }


    class AuthenticateViewModelFactory(private val repository: AuthenticateRepository) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = AuthenticateViewModel(repository) as T
    }
}
