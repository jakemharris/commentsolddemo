package com.commentsolddemo.ui.productDetails


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.commentsolddemo.Utils.PRODUCT_KEY
import com.commentsolddemo.databinding.FragmentProductDetailsBinding
import com.commentsolddemo.model.Product
import com.google.gson.Gson

class ProductDetailsFragment : Fragment() {
    private var _binding: FragmentProductDetailsBinding? = null
    private val binding get() = _binding!!
    private var mProduct: Product? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentProductDetailsBinding.inflate(inflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val jsonString = arguments?.getString(PRODUCT_KEY)
        jsonString?.let {
            mProduct = Gson().fromJson(jsonString, Product::class.java)
        }

        mProduct?.let {
            val stringData = "Name: ${it.product_name} \n\nBrand:${it.brand} \n\nDescription: ${it.description} \n\nProduct type: ${it.product_type}"
            binding.tvDetails.text = stringData
        }

    }

    companion object {
        fun newInstance() = ProductDetailsFragment()
    }
}
