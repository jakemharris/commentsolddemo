package com.commentsolddemo.model

data class NewProductResponse(
    val message: String,
    val product_id: Long
)