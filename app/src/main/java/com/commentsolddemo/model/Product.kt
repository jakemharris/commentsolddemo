package com.commentsolddemo.model

data class Product(
    val id: Long?,
    val product_name: String?,
    val description: String?,
    val style: String?,
    val brand: String?,
    val url: String?,
    val product_type: String?,
    val shipping_price: Long?,
    val note: String?
)
