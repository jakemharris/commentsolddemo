package com.commentsolddemo.model

data class NewProductDto(
    val name: String,
    val description: String,
    val style: String,
    val brand: String,
    val shipping_price_cents: Long,
)
