package com.commentsolddemo.model

data class GetProductsResponse(
    val count: Long,
    val total: Long,
    val products: List<Product>
)
