package com.commentsolddemo.model

data class AuthenticateResponse(
    val token: String,
    val error: Long
)