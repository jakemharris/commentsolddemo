package com.commentsolddemo.model

data class Inventory(
    val id: Long?,
    val product_id: Long?,
    val quantity: Long?,
    val color: String?,
    val size: String?,
    val weight: String?,
    val price_cents: Long?,
    val sale_price_cents: Long?,
    val cost_cents: Long?,
    val sku: Long?,
    val length: String?,
    val width: String?,
    val height: String?,
    val note: String?
)
